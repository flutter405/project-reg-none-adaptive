import 'package:flutter/material.dart';

class ExamTable extends StatefulWidget {
  const ExamTable({super.key});

  @override
  State<ExamTable> createState() => _ExamTable();
}

class _ExamTable extends State<ExamTable> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            ExamTableMidterm(),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amberAccent.shade400,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "EXAM TIME TABLE",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}

Widget ExamTableMidterm() {
  return Column(
    children: [
      Divider(height: 10,color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            children: [
              Text("MIDTERM", style: TextStyle(fontSize: 25))
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.arrow_drop_down,
                size: 35,
              )
            ],
          ),
        ],
      ),
      Divider(height: 25),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Software Testing (IF-3M210)',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('16 Jan 2023',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('09:00-12:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Web Programming (IF-4C02)',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('16 Jan 2023',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('17:00-20:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('OOAP (IF-11M280)',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('17 Jan 2023',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('17:00-20:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Multimedia (IF-7T01)',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('18 Jan 2023',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('13:00-16:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('NLP (IF-11M280)',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('18 Jan 2023',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('17:00-20:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Mobile Application (Online)',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('20 Jan 2023',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('13:00-16:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
    ],
  );
}
