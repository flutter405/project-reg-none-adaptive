import 'package:flutter/material.dart';

class Biography extends StatefulWidget {
  const Biography({super.key});

  @override
  State<Biography> createState() => _Biography();
}

class _Biography extends State<Biography> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            ProfileBibliography(),
            CardProfile(),
            CardOrganization(),
            CardContact(),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amberAccent.shade400,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "BIOGRAPHY",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}

Widget ProfileBibliography() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Column(
        children: [
          Icon(Icons.account_circle, size: 150, color: Colors.amber),
        ],
      ),
    ],
  );
}

Widget CardProfile() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 170,
      child: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            children: [
              columnProfile(),
              columnDataProfile(),
            ],
          )
        ],
      ),
    ),
  );
}

Widget columnProfile() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("   PROFILE",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Text("   NAME :", style: TextStyle(height: 2.7, fontSize: 13)),
      Text("   BIRTH DATE :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("   GANDER :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("   NATIONALITY :   ", style: TextStyle(height: 2.0, fontSize: 13)),
    ],
  );
}

Widget columnDataProfile() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("", style: TextStyle(height: 2.0, fontSize: 15)),
      Text("Natthakritta Nawachat",
          style: TextStyle(height: 2.7, fontSize: 13)),
      Text("25 JULY 2001", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("Female", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("Thai", style: TextStyle(height: 2.0, fontSize: 13)),
    ],
  );
}

Widget CardOrganization() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 250,
      child: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            children: <Widget>[
              columnOrganization(),
              columnDataOrganization(),
            ],
          )
        ],
      ),
    ),
  );
}

Widget columnOrganization() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("   BIOGRAPHY",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Text("   STUDENT ID :     ", style: TextStyle(height: 2.7, fontSize: 13)),
      Text("   YEAR :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("   FACULTY :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("   PROGRAM :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("   STATUS :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("   ADVISROR :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("", style: TextStyle(height: 2.0, fontSize: 13)),
    ],
  );
}

Widget columnDataOrganization() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("", style: TextStyle(height: 2.0, fontSize: 15)),
      Text("63160195", style: TextStyle(height: 2.7, fontSize: 13)),
      Text("3", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("Informatics", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("2115020: Computer Science",
          style: TextStyle(height: 2.0, fontSize: 13)),
      Text("Studying", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("MR.Pusit Kulkasem", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("Asst. Prof. Dr.Komate Amphawan",
          style: TextStyle(height: 2.0, fontSize: 13)),
    ],
  );
}

Widget CardContact() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 145,
      child: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            children: [
              columnContact(),
              columnDataContact(),
            ],
          )
        ],
      ),
    ),
  );
}

Widget columnContact() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("   CONTACT INFO",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Text("   EMAIL :", style: TextStyle(height: 2.7, fontSize: 13)),
      Text("   PHONE :", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("   ADDRESS :", style: TextStyle(height: 2.0, fontSize: 13)),
    ],
  );
}

Widget columnDataContact() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("", style: TextStyle(height: 2.0, fontSize: 15)),
      Text("63160195@go.buu.ac.th",
          style: TextStyle(height: 2.7, fontSize: 13)),
      Text("065-919-5566", style: TextStyle(height: 2.0, fontSize: 13)),
      Text("89/9 Bamnetnarong Chaiyaphum", style: TextStyle(height: 2.0, fontSize: 13)),
    ],
  );
}