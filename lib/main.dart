import 'package:device_preview/device_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'AlertDialog.dart';
import 'Biography.dart';
import 'EnrollmentResult.dart';
import 'ExamTable.dart';
import 'Graduation.dart';
import 'Scholarship.dart';

void main() {
  runApp(DevicePreview(
      enabled: true,
      builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          useInheritedMediaQuery: true,
          builder: DevicePreview.appBuilder,
          home: RegApp())));
}

class RegApp extends StatefulWidget {
  @override
  State<RegApp> createState() => _RegAppState();
}

class _RegAppState extends State<RegApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amberAccent.shade400,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.translate,
              color: Colors.white,
            ),
          ),
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "BURAPHA UNIVERSITY",
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: <Widget>[
                  Icon(Icons.account_circle, size: 100, color: Colors.amber),
                  Text('', style: TextStyle(fontSize: 10)),
                  Text('Natthakritta Nawachat')
                ],
              ),
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.person),
              title: const Text('Biography'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Biography()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(CupertinoIcons.doc),
              title: const Text('Enroll'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(CupertinoIcons.doc_checkmark),
              title: const Text('Enrollment Result'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EnrollmentResult()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.edit_calendar),
              title: const Text('Exam Timetable'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ExamTable()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.edit_note),
              title: const Text('Graduation check'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Graduation()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.attach_money),
              title: const Text('Dept/scholarship'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Scholarship()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(
                Icons.power_settings_new,
                color: Colors.red,
              ),
              title: const Text('Logout',
                  style: TextStyle(
                    color: Colors.red,
                  )),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            btnDay(),
            StudyTimeTable(),
            EnglishScore(),
            newTopic(),
          ],
        ),
      ),
    );
  }
}

Widget btnDay() {
  return Padding(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: <Widget>[
              SizedBox(
                height: 50,
                width: 80,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Text('MON'),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: 50,
                width: 60,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Text('TUE'),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: 50,
                width: 60,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.grey[350],
                  ),
                  child: Center(
                    child: Text('WED'),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: 50,
                width: 60,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Text('THU'),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: 50,
                width: 60,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Text('FRI'),
                  ),
                ),
              )
            ],
          ),
        ],
      ));
}

Widget StudyTimeTable() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Divider(height: 10, color: Colors.transparent),
      Row(
        children: <Widget>[
          Text(" Schedule", style: TextStyle(fontSize: 20, height: 1.5))
        ],
      ),
      Divider(height: 25),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Mobile Application',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-4C02',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('  10:00-12:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Multimedia', style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-4C01',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('  13:00-16:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: 80,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: 80,
                    width: 270,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Web Programming',
                                style: TextStyle(fontSize: 17))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-3C01',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            ),
                            Column(
                              children: [
                                Text('15:00-17:00',
                                    style: TextStyle(
                                        height: 2.0, color: Colors.grey))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
    ],
  );
}

Widget EnglishScore() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 205,
      child: ListView(
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          columnEnglishScore(),
        ],
      ),
    ),
  );
}

Widget columnEnglishScore() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("   ENGLISH SCORE",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Text("   *สำหรับนิสิตตั้งแต่รหัส59 เป็นต้นไปต้องสอบให้ครบ 2 ครั้ง",
          style: TextStyle(height: 2.7, fontSize: 13)),
      DataTable(columns: [
        DataColumn(
            label: Text(' คะแนนสอบครั้งที่ 1',
                textAlign: TextAlign.center, style: TextStyle(fontSize: 13))),
        DataColumn(
            label: Text(' คะแนนสอบครั้งที่ 2',
                textAlign: TextAlign.center, style: TextStyle(fontSize: 13))),
      ], rows: [
        DataRow(cells: [
          DataCell(Center(
            child: (Text('32', textAlign: TextAlign.center)),
          )),
          DataCell(Center(
            child: (Text('-', textAlign: TextAlign.center)),
          )),
        ]),
      ])
    ],
  );
}

Widget newTopic() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 320,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          columnNewTopic(),
          columnNewTopic2(),
          // columnSecondExam(),
        ],
      ),
    ),
  );
}

Widget columnNewTopic() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("   NEWS TOPIC",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Padding(
        padding: EdgeInsets.only(right: 50),
        child: Text("  1. ประเมิณความคิดเห็นต่อสำนักงานอธิการบดี",
            style: TextStyle(height: 2.5, fontSize: 13)),
      ),
      pictureNewTopic(),
    ],
  );
}

Widget pictureNewTopic() {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.only(top: 10, left: 30),
          child: Image.network(
              'https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/326509974_536050371838981_2831333027248372933_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=BmhdiOrYYHkAX9lqdKQ&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQwGa26Q7lLpE1nebmiAQLgcesG-E1faQp-DHYhdjC-gA&oe=63F4C49A',
              height: 200)),
    ],
  );
}

Widget columnNewTopic2() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(" ",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Padding(
        padding: EdgeInsets.only(right: 50),
        child: Text("  2. การทำบัตรนิสิตกับธนาคารกรุงไทย",
            style: TextStyle(height: 2.5, fontSize: 13)),
      ),
      pictureNewTopic2(),
    ],
  );
}

Widget pictureNewTopic2() {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.all(10),
          child: Image.network(
              'https://scontent.fbkk5-4.fna.fbcdn.net/v/t1.15752-9/326323529_859258131997513_5781641427720499130_n.png?_nc_cat=110&ccb=1-7&_nc_sid=ae9488&_nc_ohc=0Brd7ALe-AEAX8Cy2pY&tn=ee5827Vjg-kg1k7B&_nc_ht=scontent.fbkk5-4.fna&oh=03_AdTG85HJt8AMHO7JyIqtNeuUrJEqgupzi9OjPisnthulzA&oe=63F4C776',
              height: 200)),
    ],
  );
}
